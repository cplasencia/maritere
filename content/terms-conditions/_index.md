---
title: "Terms & Conditions"
date: 2019-10-17T11:22:16+06:00
draft: false
description : "this is a meta description"
---

### Terms and Condition

Lea estos términos y condiciones que se detallan a continuación, que describen una gran cantidad de ventajas legales, pero la conclusión es que nuestro objetivo es siempre cuidarnos a los dos, como cliente o como vendedor. Hemos incluido muchos de estos términos para protegernos legalmente, pero si tiene un problema, no dude en enviarnos un correo electrónico a contact@themefisher.com y haremos todo lo posible para resolverlo de manera justa y oportuna.

### Product Buy & delivery policy

1) Todos los pagos se procesan de forma segura a través de PayPal o Stripe. Themefisher no procesa pagos directamente a través del sitio web. Por lo tanto, cualquier tipo de problema si se enfrenta a Themefisher no asumirá ninguna responsabilidad.

### Product Buy & delivery policy

Lea estos términos y condiciones que se detallan a continuación, que describen una gran cantidad de ventajas legales, pero la conclusión es que nuestro objetivo es siempre cuidarnos a los dos, como cliente o como vendedor. Hemos incluido muchos de estos términos para protegernos legalmente, pero si tiene un problema, no dude en enviarnos un correo electrónico a contact@themefisher.com y haremos todo lo posible para resolverlo de manera justa y oportuna.

### Refund Policy

1) Dado que Themefisher ofrece productos digitales irrevocables y no tangibles, NO emitimos reembolsos después de realizar una compra. Si hay un problema con uno de los productos de descarga digital o si tiene alguna dificultad con la descarga, solucionaremos el problema.
2) Tiene 24 horas para inspeccionar su compra y determinar si no cumple con las expectativas establecidas por The Themefisher. En caso de que desee recibir un reembolso, Themefisher le emitirá un reembolso y le pedirá que especifique cómo el producto no cumplió con las expectativas.
3) Cuando realice una devolución que califique, acreditaremos el monto total, menos los cargos por manejo. Los reembolsos generalmente se procesan dentro de los 3 a 5 días hábiles posteriores a la recepción de su reclamo y determinamos que es elegible para una devolución. Los reembolsos se aplican a la opción de pago original.

### Support Policy
1) Dado que Themefisher ofrece productos digitales irrevocables y no tangibles, NO emitimos reembolsos después de realizar una compra. Si hay un problema con uno de los productos de descarga digital o si tiene alguna dificultad con la descarga, solucionaremos el problema.
2) Tiene 24 horas para inspeccionar su compra y determinar si no cumple con las expectativas establecidas por The Themefisher. En caso de que desee recibir un reembolso, Themefisher le emitirá un reembolso y le pedirá que especifique cómo el producto no cumplió con las expectativas.
3) Cuando realice una devolución que califique, acreditaremos el monto total, menos los cargos por manejo. Los reembolsos generalmente se procesan dentro de los 3 a 5 días hábiles posteriores a la recepción de su reclamo y determinamos que es elegible para una devolución. Los reembolsos se aplican a la opción de pago original.

### Why We Support:

1. El soporte cubre cómo levantarse y ayuda en la configuración del tema.
2. Ayuda para usar funciones y opciones de temas
3. Corrección de errores: si descubrió un error en el tema / plantilla, lo solucionaremos
4. Funcionalidad que no funciona como se anuncia.
